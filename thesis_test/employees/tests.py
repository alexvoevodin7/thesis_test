from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from employees.models import Department
from collections import OrderedDict


class GetNumberTests(APITestCase):
    def test_create_account(self):
        """
        Ensure we can create a new account object.
        """
        url = reverse('get-number')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {'number': 123})


class DepartmentTest(APITestCase):


    def setUp(self):
        Department.objects.create(name="Police")
        Department.objects.create(name="Medicine")


    def test_department_url(self):
        """
        Ensure we can create a new department object.
        """

        url = '/api/department/'
        data = [OrderedDict([ ('name', 'Police'), ('count', 0), ('salary_sum', 0), ('director', None), ('projects', [])]), OrderedDict([ ('name', 'Medicine'), ('count', 0), ('salary_sum', 0), ('director', None), ('projects', [])])]
        response = self.client.get(url)
        response_data = response.data
        response_data[0].pop('uid')
        response_data[1].pop('uid')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response_data, data)
